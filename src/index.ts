import * as fs from 'fs';
import * as yaml from 'js-yaml';

if (process.argv.length !== 3) {
    console.error("Usage: tidyclimate <report.yaml>");
    process.exit();
}

interface TidyDiag {
    DiagnosticName: string;
    Message: string;
    FilePath: string;
    FileOffset: number;
}

interface TidyReport {
    MainSourceFile: string;
    Diagnostics: TidyDiag[];
}

interface ClimateLines {
    begin: number;
}

interface ClimateLocation{
    path: string;
    lines: ClimateLines;
}

interface Climate {
    description: string;
    fingerprint: string;
    location: ClimateLocation; 
}

const file = process.argv[2];
const tidy_report_raw = fs.readFileSync(file, "utf8");
const tidy_report = <TidyReport[]>yaml.safeLoadAll(tidy_report_raw);
let output: Climate[] = [];


class Offset2Line {
    private cache:  { [id: string] : number[]; } = {};

    getLine(file: string, offset: number): number {
        this.loadFile(file);
        return this.doGetLine(file, offset);
    }

    private doGetLine(file: string, offset: number): number {
        const lines = this.cache[file];

        if (lines.length === 0 || lines[0] >= offset)
            return 1;

        for (let i = 1 ; i < lines.length ; ++i) {
            if (offset <= lines[i])
                return i + 1;
        }

        return 0;
    }

    private loadFile(file: string) {
        if (file in this.cache) {
            return;
        }
        
        const lines: number[] = [];
        const data = fs.readFileSync(file, 'utf8');

        for (let i = 0 ; i < data.length ; ++i) {
            if (data[i] === '\n') {
                lines.push(i);
            }
        }

        this.cache[file] = lines;
    }
}

const o2l = new Offset2Line();

for (let report of tidy_report) {
    for (let diag of report.Diagnostics) {
        output.push({
            description:  diag.DiagnosticName + ' ' + diag.Message,
            fingerprint: "",
            location: {
                path: diag.FilePath,
                lines: {
                    begin: o2l.getLine(diag.FilePath, diag.FileOffset)
                }
            }
        });
    }
}

console.log(JSON.stringify(output, null, 2));
