"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = __importStar(require("fs"));
var yaml = __importStar(require("js-yaml"));
if (process.argv.length !== 3) {
    console.error("Usage: tidyclimate <report.yaml>");
    process.exit();
}
var file = process.argv[2];
var tidy_report_raw = fs.readFileSync(file, "utf8");
var tidy_report = yaml.safeLoadAll(tidy_report_raw);
var output = [];
var Offset2Line = /** @class */ (function () {
    function Offset2Line() {
        this.cache = {};
    }
    Offset2Line.prototype.getLine = function (file, offset) {
        this.loadFile(file);
        return this.doGetLine(file, offset);
    };
    Offset2Line.prototype.doGetLine = function (file, offset) {
        var lines = this.cache[file];
        if (lines.length === 0 || lines[0] >= offset)
            return 1;
        for (var i = 1; i < lines.length; ++i) {
            if (offset <= lines[i])
                return i + 1;
        }
        return 0;
    };
    Offset2Line.prototype.loadFile = function (file) {
        if (file in this.cache) {
            return;
        }
        var lines = [];
        var data = fs.readFileSync(file, 'utf8');
        for (var i = 0; i < data.length; ++i) {
            if (data[i] === '\n') {
                lines.push(i);
            }
        }
        this.cache[file] = lines;
    };
    return Offset2Line;
}());
var o2l = new Offset2Line();
for (var _i = 0, tidy_report_1 = tidy_report; _i < tidy_report_1.length; _i++) {
    var report = tidy_report_1[_i];
    for (var _a = 0, _b = report.Diagnostics; _a < _b.length; _a++) {
        var diag = _b[_a];
        output.push({
            description: diag.DiagnosticName + ' ' + diag.Message,
            fingerprint: "",
            location: {
                path: diag.FilePath,
                lines: {
                    begin: o2l.getLine(diag.FilePath, diag.FileOffset)
                }
            }
        });
    }
}
console.log(JSON.stringify(output, null, 2));
